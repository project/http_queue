<?php

namespace Drupal\Tests\http_queue\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Widget admin test.
 *
 * @group http_queue
 */
class FunctionalTest extends BrowserTestBase {

  /**
   * Token used in test.
   *
   * @var string
   */
  private $token;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User.
   *
   * @var bool|\Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'http_queue',
  ];

  /**
   * The first tested queue.
   *
   * @var \Drupal\advancedqueue\Entity\QueueInterface
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    $this->token = md5($this->randomString());
    parent::setUp();
    $this->grantPermissions(Role::load(RoleInterface::ANONYMOUS_ID), [
      'access content',
    ]);

    $this->queue = Queue::create([
      'id' => 'first_queue',
      'label' => 'First queue',
      'backend' => 'database',
      'backend_configuration' => [
        'lease_time' => 5,
      ],
    ]);
    $this->queue->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function writeSettings(array $settings) {
    $settings['settings']['http_queue_token'] = (object) [
      'value' => $this->token,
      'required' => TRUE,
    ];
    parent::writeSettings($settings);
  }

  /**
   * Test that we can not fetch a job without the token.
   */
  public function testWrongToken() {
    // Since the token is random, we make sure its for sure not the same as
    // the one we created earlier.
    $this->drupalGet('/http-queue/get-a-job', [], $this->getHttpQueueHeaders(strrev($this->token)));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test that we can fetch a job with the token.
   */
  public function testCorrectTokenNoJobs() {
    $this->drupalGet('/http-queue/get-a-job', [], $this->getHttpQueueHeaders());
    $this->getSession()->getPage()->getContent();
    // Since there are no jobs, the status code should be 404.
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Test that we can find and claim a job if we want.
   */
  public function testCorrectTokenJobClaimed() {
    $job = Job::create('simple', ['test' => '1']);
    $this->queue->getBackend()->enqueueJobs([$job]);
    $data = $this->drupalGet('/http-queue/get-a-job', [], $this->getHttpQueueHeaders());
    $json = json_decode($data);
    $payload = json_decode($json->payload);
    $this->assertEquals('1', $payload->test);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('/http-queue/claim/' . $json->job_id, [], $this->getHttpQueueHeaders());
    $this->assertSession()->statusCodeEquals(200);
    // We should not be able to claim it again.
    $this->drupalGet('/http-queue/claim/' . $json->job_id, [], $this->getHttpQueueHeaders());
    $this->assertSession()->statusCodeEquals(400);
    // We should not be able to claim an unexisting job.
    $this->drupalGet('/http-queue/claim/' . 666, [], $this->getHttpQueueHeaders());
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Helper.
   */
  protected function getHttpQueueHeaders($token = NULL) {
    if (!$token) {
      $token = $this->token;
    }
    return [
      'x-drupal-http-queue-token' => $token,
    ];
  }

}
